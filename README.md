# Django backend combined with REST

## Includes

* Django 1.11
* Django-rest-framework 3.6.3

## Setup

```
git@gitlab.com:fanshenium/site-template-django.git <project_name>
pip install -r requirements.txt
```

## Run

```
python manage.py runserver 0:<port>
```

You can use [frontend environment](https://gitlab.com/fanshenium/site-template-env) for this project.
