from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import user_list, user_detail


urlpatterns = [
    url(r'^users/$', user_list),
    url(r'^users/(?P<pk>[0-9]+)/$', user_detail),
]

urlpatterns = format_suffix_patterns(urlpatterns)
